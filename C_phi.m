function C=C_phi(Ncl,K,LOS)

if (LOS==1)
    alpha=(1.1035 - 0.028*K -0.002*K^2 + 0.0001*K^3);
else
    alpha=1;
end

switch (Ncl)
    case 4
        CNL=0.779;
    case 5
        CNL=0.860;
    case 8
        CNL=1.018;
    case 10
        CNL=1.090;
    case 11
        CNL=1.123;
    case 12
        CNL=1.146;
    case 14
        CNL=1.190;
    case 15
        CNL=1.211;
    case 16
        CNL=1.226;
    case 19
        CNL=1.273;
    case 20
        CNL=1.289;
    otherwise
        CNL=1;
end

C=CNL*alpha;
end