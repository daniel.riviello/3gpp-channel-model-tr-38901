# 3GPP channel model TR 38901

MATLAB code for 3GPP channel model TR 38901

Authors: Daniel G. Riviello, Francesco Di Stasio, Riccardo Tuninato.
email: daniel.riviello@polito.it

Run Main_Channel_Generation.m to generate channel coefficients


