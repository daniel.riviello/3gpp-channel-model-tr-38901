function C=C_theta(Ncl,K,LOS)

if (LOS==1)
    alpha=(1.3086 + 0.0339*K - 0.0077*K^2 + 0.0002*K^3);
else
    alpha=1;
end

switch (Ncl)

    case 8
        CNL=0.889;
    case 10
        CNL=0.957;
    case 11
        CNL=1.031;
    case 12
        CNL=1.104;
    case 15
        CNL=1.1088;
    case 19
        CNL=1.184;
    case 20
        CNL=1.178;
    otherwise
        CNL=1;
end

C=CNL*alpha;
end