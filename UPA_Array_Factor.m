function [Ft, f] = UPA_Array_Factor(Ny,Nz,phi,theta, th_o, pol)

%%% Array pattern 3GPPP
theta_1 = theta;
phi_1 = phi-th_o;
theta_3db = 65; %degrees
SLA = 30; %dB
phi_3db = 65; %degrees
A_max = 30; %dB
G_max_el = 8; %dBi

A1=-1*min(12*((rad2deg(theta_1)-90)/theta_3db)^2, SLA);
A2=-1*min(12*((rad2deg(phi_1))/phi_3db)^2, A_max);

pattern_dB = -1*min(-(A1+A2), A_max);



%%%Transformation from LCS to GCS and polarization
if(pol==1)
    Ft=10^(0.05*(pattern_dB+G_max_el));
end

if(pol==2)
    Ft = zeros(2,2);
    for pp=1:2
        zeta = (2*pp-3)*pi/4; %%% -45 or +45 degree slant (cross-polarization)
        Ft(:,pp) = 10^(0.05*(pattern_dB+G_max_el)).*[cos(zeta); sin(zeta)];
    end
end


index_y = (0:Ny-1)';
index_z = (0:Nz-1)';

steer_ULAy = exp(1j*pi*index_y*sin(theta)*(sin(phi-th_o)));
steer_ULAz = exp(1j*pi*index_z*cos(theta));
f = kron(steer_ULAz, steer_ULAy);
end
