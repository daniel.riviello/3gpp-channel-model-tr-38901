clear
close all
clc
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Authors: Daniel Gaetano Riviello, Francesco Di Stasio, Riccardo Tuninato
% Contact: daniel.riviello@polito.it
% This script generate simplified channel model following the steps in
% 3GPP TR 38.901 section 7.5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Channel coefficients are contained in the Kx3 cell array H.
% Pathloss with shadowing is contained in the Kx1 vector PL. 
    
%% Parameters for channel modelling
scenario='umi';   %'umi' or 'uma'
if(strcmp(scenario,'umi')==1)
    radius = 100;
elseif(strcmp(scenario,'uma')==1)
    radius = 250;
else
    error('No valid scenario selected');
end

Area = 0.5*3*sqrt(3)*radius^2;  % Area of the Hexagon
density = 2500; %density in users/km^2

% NUMBER OF USERS %  
K = poissrnd(Area*density/1e6); % Number of users
N_sec = 3;  % Number of sectors per cell (3 for planar array)
indoor_perc= 50; % For indoor users in buildings (default = 0.1)

fc = 28e9;  % Carrier frequency in Hz
NFFT = 1024;
SCS = 66e3;
fs = SCS*NFFT;   % Sampling frequency in Hz

%%%%%%%

% Array parameters

array ='p'; % array type, c for cylindrical p for planar
pol = 1;    % single or dual polarization
% BS
Nty=8;Ntz=2;     % number of antennas in the TX array
Nt=Nty*Ntz;

% UE 1x1
Nry=2; Nrz=1;      % number of antennas in the RX array
Nr=Nry*Nrz;

c = physconst('LightSpeed');
WaveLength = c/fc;

%% Geometrical Parameters (UMi Street Canyon)
ISD = [200 500];  % Intersite Distance
radius = ISD(1)/2*(strcmp(scenario,'umi')) + ISD(2)/2*(strcmp(scenario,'uma'));     % 100 m for umi, 1000 m for uma
min_dist = 10;    % valid for umi and uma

posBS=0+1j*0;  % BS is placed in the center of the Hexagon
%according to TR 38.901 the height of the BS has to be 10 m for umi
%scenario and 25m for uma
hBS=strcmp(scenario,'umi')*10 + 25*strcmp(scenario,'uma');

UT_orient=[-pi+2*pi*rand(1,K); deg2rad(6)*randn(1,K); deg2rad(6)*randn(1,K)];

% User orientation: 
% 1) bearing angle uniformly distributed between 0 and 360
% 2) tilt angle Gaussian distributed zero mean 36 degrees variance
% 3) slant angle Gaussian distributed zero mean 36 degrees variance

[posUEs, hUEs, indoorFlag]=genPosHex(K,radius,min_dist,indoor_perc);

%% Distances between users and BS

dis2D=abs(posUEs-posBS);

posUEs3D=dis2D+1j*(hBS-hUEs);
dis3D=abs(posUEs3D);

% for indoor users

d2D_in=(25*min(rand(K,1),rand(K,1))).*indoorFlag;  % distances for indoor users from UT to building edge
d2D_out=dis2D-d2D_in;    % distance from the building edge to the BS   



%% compute AOD, ZOD, AOA, ZOA

aodLOS=rad2deg(angle(posUEs));
zodLOS=rad2deg((angle(posUEs3D)+pi/2));

%aoaLOS=-aodLOS;
aoaLOS = wrapTo180(aodLOS-sign(aodLOS-180)*180);
zoaLOS = 180-zodLOS;

%LOS_angles=struct('AOD',aodLOS,'AOA',aoaLOS,'ZOD',zodLOS,'ZOA',zoaLOS);
LOS_angles = deg2rad([aodLOS,zodLOS,aoaLOS,zoaLOS]);
%% LOS condition

% We consider the LOS/NLOS conditions given in table 7.4.2-1

hasLOS=LOS_probability(d2D_out,hUEs,scenario);   % LOS flags 

%% Large scale parameters LSP

[Ncl, Nray, LSP]=Large_Scale_Parameters...
    (fc,K,posUEs,dis2D,hBS,hUEs,indoorFlag,hasLOS,scenario,UT_orient, LOS_angles);

%% Path Loss calculation

PL=Path_Loss_calculation(fc,K,hasLOS,indoorFlag,dis2D,dis3D,...
    d2D_in,d2D_out,hBS,hUEs,scenario,LSP.SF);

%% Small scale parameters

[SSP, Ncl_eff]=Small_Scale_Parameters(K,Ncl,Nray,hasLOS,indoorFlag,LSP,...
    aodLOS,zodLOS,aoaLOS,zoaLOS,scenario);

%% Channel generation

H=cell(K,N_sec);
tau_vec = cell(K,1);
N_taps = zeros(K,1);
Sec_assignment = zeros(K,1);

for kk=1:K
    [H(kk,:), tau_vec{kk}, N_taps(kk), Sec_assignment(kk)]=Channel_generation(Nty,Ntz,Nry,Nrz,LSP.UT_or(:,kk),LSP.K(kk),...
            LSP.c_DS(kk), SSP(kk),Ncl_eff(kk),Nray(kk),pol,hasLOS(kk), LOS_angles(kk,:), fs);
end

show_plot = 1; %flag to plot the impulse response, 0 false, 1 true 
if(show_plot==1)
    %%% Plot impulse response with stem3, only coefficients with gain>0 are shown
    show_user = 10; %Select which user channel impulse response to plot from 1 to K
    H_user = H{show_user,Sec_assignment(show_user)};
    figure
    for ii=1:Nr
        H_Nr = squeeze(H_user(ii,:,:));
        H_Nr(H_Nr<=0)=nan;
        subplot(1,Nr,ii)
        title(['RX antenna '], num2str(ii));
        stem3(repmat(tau_vec{show_user}*1e6,Nt,1), repmat((1:Nt)',1,N_taps(show_user)), abs(H_Nr));
        ylabel('TX antennas');
        xlabel('delay [ms]');
        zlabel('Coefficient gain');
    end 
    string = ['Channel impulse response for user ', num2str(show_user)];
    suptitle(string); 
end
