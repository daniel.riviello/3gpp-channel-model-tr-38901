function hasLOS=LOS_probability(d2D,hUEs,scenario)
% This function calculates the LOS probabilities according with 3GPP TR
% 38.901 table 7.4.2-1
% The function returns the hasLOS flags for the K users, if the flag is 1
% the user has LOS component, otherwise its flag is 0


K=length(d2D);

PRL=zeros(K,1);

for kk=1:K
    dis=d2D(kk);
    h=hUEs(kk);
    
    if strcmp(scenario,'umi')
        PRL(kk)=1*(dis<=18)+(dis>18)*...
            (18/dis+(exp(-dis/36)*(1-18/dis)));
    else
        cht=(h>13 && h<=23)*((h-13)/10)^1.5;
        
        PRL(kk)=1*(dis<=18)+(dis>18)*...
            (18/dis+exp(-dis/63)*(1-18/dis))*...
            (1+cht*5/4*(dis/100)^3 * exp(-dis/150));
        
    end
    
end

hasLOS=binornd(ones(K,1),PRL); 
end