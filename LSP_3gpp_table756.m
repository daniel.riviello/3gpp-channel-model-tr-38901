%% parameters valid for UMI and UMA 

mu_DS_O2I=-6.62;  sigma_DS_O2I=0.32;
mu_ASD_O2I=1.25;  sigma_ASD_O2I=0.42;
mu_ASA_O2I=1.76;  sigma_ASA_O2I=0.16;
mu_ZSA_O2I=1.01;  sigma_ZSA_O2I=0.43;
sigma_SF_O2I=7;

%% parameters for scenario
if strcmp(scenario,'umi')
    % number of cluster
    Ncl_LOS=12;  Ncl_NLOS=19;  Ncl_O2I=12;
    
    % delay spread
    mu_DS_LOS=-0.24*log10(1+fc)-7.14;  
    sigma_DS_LOS=0.38;
    mu_DS_NLOS=-0.24*log10(1+fc)-6.83;  
    sigma_DS_NLOS=0.16*log10(1+fc)+0.28;
    
    
    % ASD
    mu_ASD_LOS = -0.05*log10(1+fc)+1.21;  
    sigma_ASD_LOS = 0.41;
    mu_ASD_NLOS = -0.23*log10(1+fc)+1.53;  
    sigma_ASD_NLOS = 0.11*log10(1+fc)+0.33;
    
    % ASA
    mu_ASA_LOS = -0.08*log10(1+fc)+1.73;  
    sigma_ASA_LOS = 0.014*log10(1+fc)+0.28;
    mu_ASA_NLOS = -0.08*log10(1+fc)+1.81;  
    sigma_ASA_NLOS = 0.05*log10(1+fc)+0.3;
    
    % ZSA
    mu_ZSA_LOS = -0.1*log10(1+fc)+0.73;  
    sigma_ZSA_LOS = -0.04*log10(1+fc)+0.34;
    mu_ZSA_NLOS = -0.04*log10(1+fc)+0.92;  
    sigma_ZSA_NLOS = -0.057*log10(1+fc)+0.41;
    
    % ZSD
    mu_ZSD_LOS = max(-0.21, -14.8*(d2D/1000)+0.01*abs(hBS-hUE)+0.83);  
    sigma_ZSD_LOS = 0.35;
    mu_offset_LOS=0;
    mu_ZSD_NLOS = max(-0.5, -3.1*(d2D/1000)+0.01*max((hUE-hBS),0+0.2));  
    sigma_ZSD_NLOS = 0.35;
    mu_offset_NLOS=-10.^(-1.5*(max(10,d2D) + 3.3));
    
    % K factor
    mu_K=9;
    sigma_K=5;
    
    % SF
    sigma_SF_LOS = 4;
    sigma_SF_NLOS = 7.82;
    
    % cross correlation matrix
    
    Cxx0_LOS=[1,0.5,-0.4,-0.5,-0.4,0,0;...
        0.5,1,-0.7,-0.2,-0.3,0,0;...
        -0.4,-0.7,1,0.5,0.8,0,0.2;...
        -0.5,-0.2,0.5,1,0.4,0.5,0.3;...
        -0.4,-0.3,0.8,0.4,1,0,0;...
        0,0,0,0.5,0,1,0;...
        0,0,0.2,0.3,0,0,1];
    
    Cxx0_NLOS=[1,0,-0.7,0,-0.4,0,0;...
        0,1,0,0,0,0,0;...
        -0.7,0,1,0,0.4,-0.5,0;...
        0,0,0,1,0,0.5,0.5;...
        -0.4,0,0.4,0,1,0,0.2;...
        0,0,-0.5,0.5,0,1,0;...
        0,0,0,0.5,0.2,0,1];
    
    Cxx0_O2I=[1,0,-0.5,0.2,0,0,0;...
        0,1,0,0,0,0,0,;...
        -0.5,0,1,0.4,0.4,-0.6,-0.2;...
        0.2,0,0.4,1,0,-0.2,0;...
        0,0,0.4,0,1,0,0.5;...
        0,0,-0.6,-0.2,0,1,0.5;...
        0,0,-0.2,0,0.5,0.5,1];

    
    %%% CORRELATION DISTANCES
    
    Dm_DS_LOS=7;    Dm_DS_NLOS=10;  Dm_DS_02I=10;
    Dm_ASD_LOS=8;   Dm_ASD_NLOS=10; Dm_ASD_O2I=11;
    Dm_ASA_LOS=8;   Dm_ASA_NLOS=9;  Dm_ASA_O2I=17;
    Dm_SF_LOS=10;   Dm_SF_NLOS=13;  Dm_SF_O2I=7;
    Dm_K_LOS=15;    Dm_K_NLOS=1e-5; Dm_K_O2I=1e-5;
    Dm_ZSA_LOS=12;  Dm_ZSA_NLOS=10; Dm_ZSA_O2I=25;
    Dm_ZSD_LOS=12;  Dm_ZSD_NLOS=10; Dm_ZSD_O2I=25;
    
    % Cluster delay spread
    c_DS_LOS=5; c_DS_NLOS=11;   c_DS_O2I=11;
    

else
    % number of cluster
    Ncl_LOS=12;  Ncl_NLOS=20;  Ncl_O2I=12;
    
    % delay spread
    mu_DS_LOS=-0.0963*log10(fc)-6.955;  sigma_DS_LOS=0.66;
    mu_DS_NLOS=-0.204*log10(fc)-6.28;  sigma_DS_NLOS=0.39;
    
    % ASD
    mu_ASD_LOS = 0.1114*log10(fc)+1.06;  
    sigma_ASD_LOS = 0.28;
    mu_ASD_NLOS = 0.1144*log10(fc)+1.5;  
    sigma_ASD_NLOS = 0.28;
    
    % ASA
    mu_ASA_LOS = 1.81;  
    sigma_ASA_LOS = 0.20;
    mu_ASA_NLOS = 2.08-0.27*log10(fc);  
    sigma_ASA_NLOS = 0.11;
    
    % ZSA
    mu_ZSA_LOS = 0.95;  
    sigma_ZSA_LOS = 0.16;
    mu_ZSA_NLOS = -0.3236*log10(fc)+1.512;  
    sigma_ZSA_NLOS = 0.16;
    
    % ZSD
    mu_ZSD_LOS = max(-0.5, -2.1*(d2D/1000)-0.01*(hUE-1.5)+0.75);
    sigma_ZSD_LOS = 0.40;
    mu_offset_LOS=0;
    mu_ZSD_NLOS = max(-0.5, -2.1*(d2D/1000)-0.01*(hUE-1.5)+0.3);
    sigma_ZSD_NLOS = 0.49;
    
    afc=0.208*log10(fc)-0.782;
    bfc=25;
    cfc=-0.13*log10(fc)+2.03;
    efc=7.66*log10(fc)-5.96;
    
    mu_offset_NLOS=efc-10.^(afc.*log10(max(bfc,d2D))+cfc-0.7*(hUE-1.5));
    
    % K factor
    mu_K=9;
    sigma_K=3.5;
    
    % SF
    sigma_SF_LOS = 4;
    sigma_SF_NLOS = 6;
    
    Cxx0_LOS=[1,0,-0.4,-0.5,-0.5,0,-0.8;...
        0,1,-0.4,0,-0.2,0,0;...
        -0.4,-0.4,1,0.4,0.8,-0.2,0;...
        -0.5,0,0.4,1,0,0.5,0;...
        -0.5,-0.2,0.8,0,1,-0.3,0.4;...
        0,0,-0.2,0.5,-0.3,1,0;...
        -0.8,0,0,0,0.4,0,1];
    
    Cxx0_NLOS=[1,0,-0.4,-0.6,0,0,-0.4;...
        0,1,0,0,0,0,0;...
        -0.4,0,1,0.4,0.6,-0.5,0;...
        -0.6,0,0.4,1,0.4,0.5,-0.1;...
        0,0,0.6,0.4,1,0,0;...
        0,0,-0.5,0.5,0,1,0;...
        -0.4,0,0,-0.1,0,0,1];
    
    Cxx0_O2I=[1,0,-0.5,0.2,0,0,0;...
        0,1,0,0,0,0,0,;...
        -0.5,0,1,0.4,0.4,-0.6,-0.2;...
        0.2,0,0.4,1,0,-0.2,0;...
        0,0,0.4,0,1,0,0.5;...
        0,0,-0.6,-0.2,0,1,0.5;...
        0,0,-0.2,0,0.5,0.5,1];
    
    %%% CORRELATION DISTANCES
    
    Dm_DS_LOS=30;    Dm_DS_NLOS=40;  Dm_DS_02I=10;
    Dm_ASD_LOS=15;   Dm_ASD_NLOS=50; Dm_ASD_O2I=11;
    Dm_ASA_LOS=15;   Dm_ASA_NLOS=50;  Dm_ASA_O2I=17;
    Dm_SF_LOS=37;   Dm_SF_NLOS=50;  Dm_SF_O2I=7;
    Dm_K_LOS=12;    Dm_K_NLOS=1e-5; Dm_K_O2I=1e-5;
    Dm_ZSA_LOS=15;  Dm_ZSA_NLOS=50; Dm_ZSA_O2I=25;
    Dm_ZSD_LOS=15;  Dm_ZSD_NLOS=50; Dm_ZSD_O2I=25;
    
    % Cluster delay spread
    c_DS_LOS=max(0.25,6.5622-3.4084*log10(fc)); 
    c_DS_NLOS=max(0.25,6.5622-3.4084*log10(fc));  
    c_DS_O2I=11;
    
end

mu_tot = [0 0 0; mu_K 0 0; mu_DS_LOS mu_DS_NLOS mu_DS_O2I;...
    mu_ASD_LOS mu_ASD_NLOS mu_ASD_O2I; mu_ASA_LOS mu_ASA_NLOS mu_ASA_O2I;...
    0 0 0; mu_ZSA_LOS mu_ZSA_NLOS mu_ZSA_O2I];

sigma_tot = [sigma_SF_LOS sigma_SF_NLOS sigma_SF_O2I; sigma_K 0 0; sigma_DS_LOS sigma_DS_NLOS sigma_DS_O2I;...
    sigma_ASD_LOS sigma_ASD_NLOS sigma_ASD_O2I; sigma_ASA_LOS sigma_ASA_NLOS sigma_ASA_O2I;...
    0 0 0; sigma_ZSA_LOS sigma_ZSA_NLOS sigma_ZSA_O2I];   


mu_ZSD_tot=[mu_ZSD_LOS, mu_ZSD_NLOS];
sigma_ZSD_tot=[sigma_ZSD_LOS,sigma_ZSD_NLOS];

Cxx0 = cell(3,1);
Cxx0{1} = sqrtm(Cxx0_LOS);
Cxx0{2} = sqrtm(Cxx0_NLOS);
Cxx0{3} = sqrtm(Cxx0_O2I);
