function [pos, hUEs, indoorFlag]=genPosHex(K,R,Rmin,indoor_perc)


K_indoor=floor(K*indoor_perc/100);

Max_floor=8;

Nfloor=randi([4 Max_floor],K_indoor,1);
floorUE=ones(K,1);

for kk=1:K_indoor
    floorUE(kk)=randi([1 Nfloor(kk)],1);
end


hUEs=1.5+3*(floorUE-1);

indoorFlag=zeros(K,1);
indoorFlag(1:K_indoor)=1;

% This function generate the 2D position for K-User in a hexagon of radius
% R

pos=zeros(K,1);
ii=1;

while ii<=K
    a = -0.75*R+1.5*R*rand;
    b = -0.25*R+0.5*R*rand;
    x = a + b;
    y = (abs(x)<0.5*R)*(-R*sqrt(3)/2+sqrt(3)*R*rand)+(abs(x)>0.5*R)*...
        (sqrt(3)*abs(x)-R*sqrt(3)+(2*sqrt(3)*R-2*sqrt(3)*abs(x)).*rand);
    pos_temp = round(x+1i*y, 2);
    
    if (abs(pos_temp)>Rmin)
        flag = find(pos==pos_temp);
        if isempty(flag)
            pos(ii) = pos_temp;
            ii=ii+1;
        else
            if isempty(find(h_UE(flag) == h_UE(ii)))
                pos(ii) = pos_temp;
                ii=ii+1;
            end
        end
            
    end
end