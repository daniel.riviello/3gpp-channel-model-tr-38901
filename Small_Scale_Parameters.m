function [SCP, Ncl_eff]=Small_Scale_Parameters(K,Ncl,Nray,hasLOS,indoor,LSP,...
    aodLOS,zodLOS,aoaLOS,zoaLOS,scenario)

%%% parameters
r=1.5;

P_threshold_dB=-25;
P_threshold=10^(0.1*P_threshold_dB);

alpha_m=[0.0447,-0.0447,0.1413,-0.1413,0.2492,-0.2492,...
    0.3715,-0.3715,0.5129,-0.5129,0.6797,-0.6797,0.8844,-0.8844,...
    1.1481,-1.1481,1.5195,-1.5195,2.1551,-2.1551];

if strcmp(scenario,'umi')
    shad_LOS=3;
    shad_NLOS=3;
    shad_O2I=4;
    
    rt_LOS=3;
    rt_NLOS=2.1;
    rt_O2I=2.2;
    
    cASD_LOS=3;
    cASD_NLOS=10;
    cASD_O2I=5;
    
    cASA_LOS=17;
    cASA_NLOS=22;
    cASA_O2I=8;
    
    cZSA_LOS=7;
    cZSA_NLOS=7;
    cZSA_O2I=3;
    
    cZSD_LOS=7;
    cZSD_NLOS=7;
    cZSD_O2I=3;
    
    muX_LOS=9;
    muX_NLOS=8;
    muX_O2I=9;
    
    sigmaX_LOS=3;
    sigmaX_NLOS=3;
    sigmaX_O2I=5;
else
    shad_LOS=3;
    shad_NLOS=3;
    shad_O2I=4;
    
    rt_LOS=2.5;
    rt_NLOS=2.3;
    rt_O2I=2.2;
    
    cASD_LOS=5;
    cASD_NLOS=2;
    cASD_O2I=5;
    
    cASA_LOS=11;
    cASA_NLOS=15;
    cASA_O2I=8;
    
    cZSA_LOS=7;
    cZSA_NLOS=7;
    cZSA_O2I=3;
    
    cZSD_LOS=7;
    cZSD_NLOS=7;
    cZSD_O2I=3;
    
    muX_LOS=8;
    muX_NLOS=7;
    muX_O2I=9;
    
    sigmaX_LOS=4;
    sigmaX_NLOS=3;
    sigmaX_O2I=5;
end



%%% initialization
tau=cell(K,1);
Pn=cell(K,1);
AOD=cell(K,1);
AOA=cell(K,1);
ZOD=cell(K,1);
ZOA=cell(K,1);
Ncl_eff=zeros(K,1);
kappa = cell(K,1);

%% Common parameters

rt=(indoor==1)*rt_O2I+(indoor==0).*((hasLOS==1)*rt_LOS+(hasLOS==0)*rt_NLOS);
shad=(indoor==1)*shad_O2I+(indoor==0).*...
    ((hasLOS==1)*shad_LOS+(hasLOS==0)*shad_NLOS);

cASD=(indoor==1)*cASD_O2I+(indoor==0).*...
    ((hasLOS==1)*cASD_LOS+(hasLOS==0)*cASD_NLOS);

cASA=(indoor==1)*cASA_O2I+(indoor==0).*...
    ((hasLOS==1)*cASA_LOS+(hasLOS==0)*cASA_NLOS);

cZSD=(indoor==1)*cZSD_O2I+(indoor==0).*...
    ((hasLOS==1)*cZSD_LOS+(hasLOS==0)*cZSD_NLOS);

cZSA=(indoor==1)*cZSA_O2I+(indoor==0).*...
    ((hasLOS==1)*cZSA_LOS+(hasLOS==0)*cZSA_NLOS);


muX=(indoor==1)*muX_O2I + (indoor==0).*...
    ((hasLOS==1)*muX_LOS + (hasLOS==0)*muX_NLOS);

sigmaX=(indoor==1)*sigmaX_O2I + (indoor==0).*...
    ((hasLOS==1)*sigmaX_LOS + (hasLOS==0)*sigmaX_NLOS);

for kk=1:K
    N=Ncl(kk);
    
    %%%% DELAYS
    tau1=-rt(kk)*LSP.DS(kk)*log(rand(N,1));
    
    mtau=min(tau1);
    tau2=sort(tau1-mtau);
    
    if (hasLOS(kk)==1)
        K_dB = 10*log10(LSP.K(kk));
        C_tau = 0.7705-0.0433*K_dB+0.0002*K_dB^2+0.000017*K_dB^3;
        tau{kk} = tau2./C_tau;
    else
        tau{kk} = tau2;
    end
    
    %%%% POWERS
    Zn=normrnd(0,shad(kk),N,1);
    Pn1=exp(-tau2*(rt(kk)-1)/(rt(kk)*LSP.DS(kk))).*10.^(-0.1*Zn);
    
    Pnorm1=sum(Pn1);
    Pn1_norm=Pn1/Pnorm1;    
    
    Pn1dB=10*log10(Pn1_norm);
    Pn2dB=Pn1dB-max(Pn1dB);
    
    Pn_surv=zeros(size(Pn2dB))+(Pn2dB>P_threshold_dB);
    
    Pn2=Pn1_norm.*Pn_surv;

    % cluster remuval
    PnFin=nonzeros(Pn2);
    Neff=length(PnFin);
    Ncl_eff(kk)=Neff;
    
    Pn{kk}=PnFin;
    
    if (hasLOS(kk)==1)
        KR = LSP.K(kk);
        PnFin=PnFin*1/(1+KR);
        P_1LOS = KR/(KR+1);
        PnFin(1)=PnFin(1)+P_1LOS;
        
    end
    
    %%%---- Angles of arrival -----
    M=Nray(kk);
    
    %%% AOA per cluster
    AOA{kk}=zeros(Neff,M);
    Ct=C_phi(N,LSP.K(kk),hasLOS(kk));
    
    AOAn_1=1/Ct*2*(LSP.ASA(kk)/1.4)*sqrt(-log(PnFin/max(PnFin))); 
    
    Xn=2*randi([0 1],Neff,1)-1;
    Yn=normrnd(0,(LSP.ASA(kk)/7)^2,Neff,1);
    
    if (hasLOS(kk)==0)
        AOAn=Xn.*AOAn_1+Yn+aoaLOS(kk);
    else
        AOAn=(Xn.*AOAn_1+Yn)-(Xn(1)*AOAn_1(1)+Yn(1)-aoaLOS(kk));
    end
    
    %%% AOAs for each ray of the cluster
    
    for nn=1:Neff
        AOA{kk}(nn,:)=AOAn(nn)+cASA(kk)*alpha_m;
    end
    
    
    %%%% Angles of departure
    AOD{kk}=zeros(Neff,M);
   
    AODn_1=1/Ct*2*(LSP.ASD(kk)/1.4)*sqrt(-log(PnFin/max(PnFin)));
    
    Xn=2*randi([0 1],Neff,1)-1;
    Yn=normrnd(0,(LSP.ASD(kk)/7)^2,Neff,1);
    
    if (hasLOS(kk)==0)
        AODn=Xn.*AODn_1+Yn+aodLOS(kk);
    else
        AODn=(Xn.*AODn_1+Yn)-(Xn(1)*AODn_1(1)+Yn(1)-aodLOS(kk));
    end
    
    %%% AODs for each ray of the cluster
    
    for nn=1:Neff
        AOD{kk}(nn,:)=AODn(nn)+cASD(kk)*alpha_m;
    end
    
    
    %%%----- Zeniths of arrival -----
    
    ZOA{kk}=zeros(Neff,M);
    
    Ch=C_theta(N,LSP.K(kk),hasLOS(kk));
    
    ZOAn_1=-1/Ch*LSP.ZSA(kk)*log(PnFin/max(PnFin));
    
    Xn=2*randi([0 1],Neff,1)-1;
    Yn=normrnd(0,(LSP.ZSA(kk)/7)^2,Neff,1);
    
    theta_off=(indoor(kk)==1)*zoaLOS(kk)+(indoor(kk)==0)*90;
    
    if (hasLOS(kk)==0)
        ZOAn=Xn.*ZOAn_1+Yn+theta_off;
    else
        ZOAn=(Xn.*ZOAn_1+Yn)-(Xn(1)*ZOAn_1(1)+Yn(1)-zoaLOS(kk));
    end
    
    for nn=1:Neff
        ZOA{kk}(nn,:)=ZOAn(nn)+cZSA(kk)*alpha_m;
    end
    
    %%% ZSD
    
    ZOD{kk}=zeros(Neff,M);
    
    ZODn_1=-1/Ch*LSP.ZSD(kk)*log(PnFin/max(PnFin));
    
    Xn=2*randi([0 1],Neff,1)-1;
    Yn=normrnd(0,(LSP.ZSD(kk)/7)^2,Neff,1);
  
    ZODn=Xn.*ZODn_1+Yn+zodLOS(kk)+LSP.mu_offset_ZOD(kk);
    
    for nn=1:Neff
        ZOD{kk}(nn,:)=ZODn(nn)+(3/8)*(10^LSP.mu_ZSD_tot(kk))*alpha_m;
    end
    
    
    %% Grad to rad
    
    AOD{kk}=deg2rad(AOD{kk});
    AOA{kk}=deg2rad(AOA{kk});
    ZOD{kk}=deg2rad(ZOD{kk});
    ZOA{kk}=deg2rad(ZOA{kk});

    %% kappa generation for polarization
    kappa{kk} = zeros(Neff,M);
    
    kappa_app = normrnd(muX(kk),sigmaX(kk),Neff,M);
    
    kappa{kk} = 10.^(0.1*kappa_app);

end

Cluster_angles = [AOAn AODn ZOAn ZODn];

SCP=struct('tau',tau,'AOD',AOD,'AOA',AOA,'ZOD',ZOD,'ZOA',ZOA,'Pn',Pn,'XPR',kappa,'C_angles', Cluster_angles);
end

