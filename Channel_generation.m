function [H, time, N_taps, best_sec]=Channel_generation(Nty,Ntz,Nry,Nrz,ut_or,K_factor,c_DS,SSP,N,M,pol,hasLOS,LOS_angles,fs)

Nt=Nty*Ntz;
Nr=Nry*Nrz;
N_sec = 3;
th_o=2*pi/3*(-1:1);

strong_cl = [SSP.tau(1) SSP.tau(2)];
subclusters = zeros(3,2);
for ii=1:2
    subclusters(1,ii) = strong_cl(ii);
    subclusters(2,ii) = strong_cl(ii)+1.28*c_DS;
    subclusters(3,ii) = strong_cl(ii)+2.56*c_DS;
end
delays = [subclusters(:); SSP.tau(3:end)];
ts = 1/fs;
N_taps = round(max(delays)/ts);
time = (0:N_taps)*ts;
edges = [time-ts/2 time(end)+ts/2];
if(sum(isnan(edges))>0)
    disp('SSP.tau');
    disp(SSP.tau);
    return;
end
[~, ~, tap_idx] = histcounts(delays, edges);
if(length(find(tap_idx))<length(tap_idx))
    disp('SSP.tau');
    disp(SSP.tau);
    disp('delays');
    disp(delays);
    disp('N_taps');
    disp(N_taps);
    disp(tap_idx);
    return;
end
N_taps = N_taps+1;

subcl_taps = reshape(tap_idx(1:6), 3, 2);
cl_taps = [subcl_taps(1,1); subcl_taps(1,2); tap_idx(7:end)];
R1 = [1:8 19 20];
R2 = [9:12 17 18];
R3 = 13:16;

H = cell(1,3);
if pol==1
    H(:)={zeros(Nr,Nt,N_taps)};
    in_ph = 2*pi*rand(N,M)-pi;
else
    H(:)={zeros(2*Nr,2*Nt,N_taps)};
    in_ph = 2*pi*rand(N,M,4)-pi;
end

%


% Compute H NLOS
for nn=1:N  % N clusters
    for mm=1:M  % M rays
        path_gain=sqrt(SSP.Pn(nn)/M);
        if(pol==2)
            pol_mat=[exp(1j*in_ph(nn,mm,1)) sqrt(1/SSP.XPR(nn,mm))*exp(1j*in_ph(nn,mm,2));...
                sqrt(1/SSP.XPR(nn,mm))*exp(1j*in_ph(nn,mm,3)) exp(1j*in_ph(nn,mm,4))];
        end
        [Fr, ar]=UT_Array_Factor(Nry,Nrz,SSP.AOA(nn,mm),SSP.ZOA(nn,mm), ut_or, pol);
        for ss = 1:N_sec
            [Ft, at]=UPA_Array_Factor(Nty,Ntz,SSP.AOD(nn,mm),SSP.ZOD(nn,mm), th_o(ss), pol);
            if(pol==1)
                actual_term=exp(1j*in_ph(nn,mm))*Ft*Fr*path_gain*ar*at';
            else
                actual_term=path_gain*kron((ar*at'),(Fr'*pol_mat*Ft));
            end
            if(nn==1 || nn==2)
                sub_idx = ismember(mm,R1)+2*ismember(mm,R2)+3*ismember(mm,R3);
                H{ss}(:,:,subcl_taps(sub_idx, nn))=H{ss}(:,:,subcl_taps(sub_idx, nn))+actual_term;
            else
                H{ss}(:,:,cl_taps(nn))=H{ss}(:,:,cl_taps(nn))+actual_term;
            end
        end
                
    end
end

if (hasLOS==1)
    H_LOS = cell(1,3);
    if (pol==1)
        H_LOS(:)={zeros(Nr,Nt)};
    else
        H_LOS={zeros(2*Nr,2*Nt)};
        pol_mat=[1 0; 0 -1];
    end
    [Fr, ar]=UT_Array_Factor(Nry,Nrz,LOS_angles(3),LOS_angles(4), ut_or, pol);
    for ss=1:N_sec
        [Ft, at]=UPA_Array_Factor(Nty,Ntz,LOS_angles(1),LOS_angles(2), th_o(ss), pol);
        H{ss} = H{ss}.*sqrt(1./(K_factor+1));
        if(pol==1)
            H_LOS{ss}(:,:)=sqrt(K_factor./(K_factor+1)).*(Ft*Fr*ar*at');
        else
            H_LOS{ss}(:,:) = sqrt(K_factor./(K_factor+1)).*kron((ar*at'), (Fr'*pol_mat*Ft));
        end
    
        H_temp = H{ss}(:,:,1)+H_LOS{ss};
        H{ss}(:,:,1) = H_temp;
    end
end

norms = zeros(1,3);
for ss=1:N_sec
    norms(ss) = norm(H{ss}(:))^2;
end
[~, best_sec] = max(norms);
end