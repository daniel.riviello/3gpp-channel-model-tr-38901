function PL =  Path_Loss_calculation(fc,K,hasLOS,indoorFlag,d2D,d3D,...
    d2Din,d2Dout,hBS,hUE,scenario,sigmaSF)

c=physconst('LightSpeed');

%%%% PATH LOSS parameters

if strcmp(scenario,'umi')
    % LOS PARAMETERS
    a1=32.4;  b1=21;
    a2=32.4;  b2=40;  c2=9.5;
    
    % NLOS PARAMETERS
    p1=22.4;  q1=35.3;  r1=21.3;  s1=0.3;
    
else
    % LOS PARAMETERS
    a1=28;  b1=22;
    a2=28;  b2=40;  c2=9;
       
    % NLOS PARAMETERS
    
    p1=13.54;  q1=39.08;  r1=20;  s1=0.6;
    
end


%%%% BreakingPoint distances
hE=ones(K,1);

if strcmp(scenario,'uma')
    gg=(d2D>18).*(5/4*(d2D/100).^3 .* exp(-d2D/100));

    cc=(hUE>=13 & hUE<23).*gg.*((hUE-13)/10).^1.5;
    
    prob1=1./(1+cc);
    
    hEtemp=binornd(ones(K,1),prob1);
    
    hE=hEtemp+(hEtemp==0).*(hUE-1.5);
end

dBP=4.*(hBS-hE).*(hUE-hE)*fc/c;


%%% Path Loss calculation
PLdB=zeros(K,1);


PL1=a1+b1*log10(d3D)+20*log10(fc*1e-9);
PL2=a2+b2*log10(d3D)+20*log10(fc*1e-9)-c2*log10(dBP.^2 + (hBS-hUE).^2);

PL_LOS=(d2D<=dBP).*PL1 + (d2D>dBP).*PL2;

PL_NLOS=p1+q1*log10(d3D)+r1*log10(fc*1e-9)-s1*(hUE-1.5);

for kk=1:K
    if hasLOS(kk)==1
        PLdB(kk)=PL_LOS(kk)+normrnd(0,sigmaSF(kk));
    else
        PLdB(kk)=max(PL_LOS(kk),PL_NLOS(kk))+normrnd(0,sigmaSF(kk));
    end
end

%%%% Outdoor to indoor losses

Lglass=2+0.2*(fc*1e-9);
Lconcrete=5+4*(fc*1e-9);

PL_tw=5-10*log10(0.3*10^(-0.1*Lglass)+0.7*10^(-0.1*Lconcrete));
PL_in=0.5*d2Din;

PL_O2I=PL_tw+PL_in+normrnd(0,4.4,K,1);

PLdB=PLdB+indoorFlag.*PL_O2I;

PL=10.^(-0.1*PLdB);
end
