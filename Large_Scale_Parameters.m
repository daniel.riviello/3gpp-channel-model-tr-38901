function [Ncl, M, LSP]=Large_Scale_Parameters(f,K,posUEs,...
    d2D,hBS,hUE,indoorFlag,hasLOS,scenario, UT_orient, LOS_angles)

% This function returns the large scale parameters, which are:
% 1- SF-> Shadowing Fading
% 2- K-> Ricean factor for LOS links
% 3- DS-> Delay spread
% 4- ASD
% 5- ASA
% 6- ZSD
% 7- ZSA
% Moreover the function returns the number of cluster per link Ncl 
% and the number of rays per cluster M

% Input parameters are:
% f- carrier frequency in Hz
% K- Number of users
% D2D 
% hBS
% hUE
% indoorFlag
% hasLOS 
% scenario
% UsersFloor

fc=f*1e-9;  % convert the carrier freq in GHz

%%%% PARAMETERS FROM 7.5-6
LSP_3gpp_table756;

%%%% Number of clusters

Ncl=(indoorFlag==1)*Ncl_O2I + (indoorFlag==0).*...
    ((hasLOS==1)*Ncl_LOS+(hasLOS==0)*Ncl_NLOS);


%%%% Cluster delay spread

c_DS=(indoorFlag==1)*c_DS_O2I + (indoorFlag==0).*...
    ((hasLOS==1)*c_DS_LOS+(hasLOS==0)*c_DS_NLOS);
c_DS = c_DS*1e-9;
%%%% Numeber of rays per cluster

M=20*ones(K,1);



xi = randn(7,K);
TLSPs = zeros(7,K);
LSPs=zeros(7,K);
idx = (indoorFlag==1)*3 + (indoorFlag==0).*...
    ((hasLOS==1)*1+(hasLOS==0)*2);

idxLOS=(hasLOS==1)*1+(hasLOS==0)*2;
for ii=1:K
    TLSPs(:,ii) = Cxx0{idx(ii)}*xi(:,ii);
    
    % SF,K,DS,ASD,ASA,ZSD,ZSA
    for jj=1:7
        if (jj==1 || jj==2)
            LSPs(jj,ii)= 10^(0.1*(mu_tot(jj,idx(ii))+sigma_tot(jj,idx(ii))*TLSPs(jj,ii)));
        elseif (jj==6)
            % it is the ZSD
            LSPs(jj,ii)=10^(mu_ZSD_tot(ii,idxLOS(ii))+ sigma_ZSD_tot(idxLOS(ii))...
                *TLSPs(jj,ii));
        else
            LSPs(jj,ii)=10^(mu_tot(jj,idx(ii))+sigma_tot(jj,idx(ii))*TLSPs(jj,ii));
        end
    end
    
end
  
% for ZSD indoor parameters are the same that outdoor, following LOS
% conditions and they depends on the user

SF=LSPs(1,:)';
K_factor=LSPs(2,:)'.*hasLOS;
DS=LSPs(3,:)';
ASD=min(LSPs(4,:)', 104);
ASA=min(LSPs(5,:)', 104);
ZSD=min(LSPs(6,:)', 52);
ZSA=min(LSPs(7,:)', 52);

LOS_angles = rad2deg(LOS_angles);
%%% ZOD spread
mu_ZSD_tot2=(hasLOS==1).*mu_ZSD_LOS +(hasLOS==0).*mu_ZSD_NLOS;
mu_offset_ZOD=(hasLOS==0).*mu_offset_NLOS;
LSP=struct('SF',SF,'K',K_factor,'DS',DS,'ASD',ASD,'ASA',ASA,'ZSA',ZSA,'ZSD',ZSD,...
    'mu_offset_ZOD',mu_offset_ZOD,'mu_ZSD_tot',mu_ZSD_tot2, 'UT_or', UT_orient,'c_DS',c_DS, 'LOS_angles', LOS_angles);
end