function [Fr, f] = UT_Array_Factor(Ny,Nz,phi,theta,UT_or,pol)

%%%Transformation from LCS to GCS and polarization
alpha = UT_or(1); %bearing angle
beta = UT_or(2);  %tilt
gamma = UT_or(3); %slant
    
theta_1 = acos(cos(beta)*cos(gamma)*cos(theta)+...
        (sin(beta)*cos(gamma)*cos(phi-alpha)-sin(gamma)*sin(phi-alpha))*sin(theta));
phi_1 = angle((cos(beta)*sin(theta)*cos(phi-alpha)-sin(beta)*cos(theta))+...
        1j*(cos(beta)*sin(gamma)*cos(theta)+(sin(beta)*sin(gamma)*cos(phi-alpha)+cos(gamma)*sin(phi-alpha))*sin(theta)));
psi = angle((sin(gamma)*cos(theta)*sin(phi-alpha)+cos(gamma)*...
      (cos(beta)*sin(theta)-sin(beta)*cos(theta)*cos(phi-alpha)))+1j*(sin(gamma)*cos(phi-alpha)+...
      sin(beta)*cos(gamma)*sin(phi-alpha)));
U = [cos(psi) -sin(psi); sin(psi) cos(psi)];


if(pol==1)
    Fr = U(1,1)*10^0.25*cos(pi/2*cos(theta_1))./sin(theta_1); %5dB gain 
end

if(pol==2)
     Fr = zeros(2,2);
     for pp=1:2
        zeta = (2*pp-3)*pi/4;
        Fr1 = 10^0.25*cos(pi/2*cos(theta_1))./sin(theta_1).*[cos(zeta); sin(zeta)]; %5 dB gain
        Fr(:,pp) = U*Fr1;
     end   
end


index_y = (0:Ny-1)';
index_z = (0:Nz-1)';

steer_ULAy = exp(1j*pi*index_y*sin(theta_1)*(sin(phi_1)));
steer_ULAz = exp(1j*pi*index_z*cos(theta_1));
f = kron(steer_ULAz, steer_ULAy);
end